<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentApprovedBy extends Model
{
    protected $fillable = [
        'user_id', 'payment_history_id'
    ];
}
