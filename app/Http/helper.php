<?php

use AfricasTalking\SDK\AfricasTalking;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

function encodeId($value)
{
    return Hashids::encode($value);
}

function decodeId($value)
{
    return Hashids::decode($value)[0];
}

function generateRandomString($len)
{
    $alphabet = '1234567890adcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $id = array();
    $alphaLength = strlen($alphabet) - 1;
    for ($i = 0; $i < $len; $i++) {
        $p = mt_rand(0, $alphaLength);
        $id[] = $alphabet[$p];
    }
    return implode($id);
}

function storeBase64Image($path, $data)
{
    $data = base64_decode($data);
    $filename = $path.generateRandomString(18).".png";
    Storage::disk('public')->put($filename, $data);
    return $filename;
}

function sendPushNotification(array $data, array $ids)
{
    $url = 'https://fcm.googleapis.com/fcm/send';

    $fields = array(
        'registration_ids' => $ids,
        'data' => $data
    );
    $fields = json_encode($fields);

    $headers = array(
        'Authorization: key=' . "AAAAKRikO8g:APA91bGStIHyhZmwQfDQfc297bquqztGC8MRSxbUwD-0_dM2-ytk9A3G_LgEoOkxuJxaTK3dZoFsfIL2KOcxEr9ON5liW9l-De5cqqQ-7lP3R7kbllFHZvdgcaGPWpGHn1ing_SPEB9E",
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    $result = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($result);
    return ($data->failure == 0) ? "success" : "error";
}

function calcExpectedConditionPeriod($num, $period)
{
    switch ($period) {
        case 'day':
            return Carbon::now()->addDays($num)->format("Y-m-d");
        case 'week':
            return Carbon::now()->addWeeks($num)->format("Y-m-d");
        case 'month':
            return Carbon::now()->addMonths($num)->format("Y-m-d");
        case 'year':
            return Carbon::now()->addYears($num)->format("Y-m-d");
    }

    return null;
}

function sendSms($message, $number)
{
    $username = env('AFRITALKUSERDEV'); // use 'sandbox' for development in the test environment
    $apiKey = env('AFRITALKDEV'); // use your sandbox app API key for development in the test environment
    $AT = new AfricasTalking($username, $apiKey);

    // Get one of the services
    $sms = $AT->sms();

    try{
        $data = [
            'to' => reformatNumber($number),
            'message' => $message
        ];
        // Use the service
        $result = $sms->send($data);
    }catch (Exception $exception) {

    }
}


function formatDate($date, $format = "d/M/Y")
{
    return Carbon::parse($date)->format($format);
}

function happeningIn($originalDate)
{
    return Carbon::parse($originalDate)->diffForHumans(Carbon::now(), true);
}

function reformatNumber($number)
{
    $number = trim($number);
    $ptn = "/^0/";  // Regex/
    $rpltxt = "+256";  // Replacement string
    return preg_replace($ptn, $rpltxt, $number);
}

function thousandsFormat($num) {

    if($num>1000) {

        $x = round($num);
        $x_number_format = number_format($x);
        $x_array = explode(',', $x_number_format);
        $x_parts = array('k', 'm', 'b', 't');
        $x_count_parts = count($x_array) - 1;
        $x_display = $x;
        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
        $x_display .= $x_parts[$x_count_parts - 1];

        return $x_display;

    }

    return $num;
}