<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\JobOrder;
use App\ProblemReport;

class AdminController extends Controller
{
    public function getDataForHome()
    {
        $customers = Customer::orderBy("id", "desc")->get();
        $jobOrders = JobOrder::orderBy("id", "desc")->get();
        $problemReports = ProblemReport::orderBy("id", "desc")->get();

        return response()->json([
            "customers" => $customers,
            "joborders" => $jobOrders,
            "reports" => $problemReports
        ]);
    }
}
