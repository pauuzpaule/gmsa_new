<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Customer;
use App\JobOrder;
use App\Car;
use App\User;
use App\ProblemReport;

class UserController extends Controller
{
    public function login(Request $request)
    {
        //customer 
        $customer = Customer::where("contact", $request->phone)->where("customer_token", $request->password)->first();
        if ($customer) {
            $customer->update([
                "fcm_token" => $request->token
            ]);
            $data = collect($customer->toArray())->put("loginAs", "customer");
            return response()->json($data);
        } 

        if (Auth::attempt(['email' => $request->phone, 'password' => $request->password])) {
            $user = User::where("email", $request->phone)->first();
            $user->update([
                "fcm_token" => $request->token
            ]);
            $data = collect($user->toArray())->put("loginAs", "admin");
            return response()->json($data);
        }

        return response()->json(["status" => "fail"]);
    }

    public function getCarz(Request $request)
    {
        $customer = Customer::find($request->customer_id);
        if ($customer != null) {
            $carz = $customer->carz()->select(["id", "model", "type", "license_plate"])->get();
            return response()->json($carz);
        }

        return response()->json([]);
    }

    public function getCarJobOrders(Request $request)
    {
        $car = Car::find($request->car_id);
        return response()->json($car->jobOrders);
    }

    public function getJobOrder(Request $request)
    {
        $jobOrder = JobOrder::with('assessment', 'invoice')->where("id", $request->job_order_id)->first();
        return response()->json($jobOrder);
    }

    public function reportProblem(Request $request) {
        $fileName = "";
        $data = $request->all();
        if($request->has("image")) {
            $fileName = storeBase64Image('customer/report/', $request->image);
            $data["image"] = $fileName;
        }
        
        ProblemReport::create($data);
        $customer = Customer::find($request->customer_id);
        $user = User::find(1);
        $message = [
            "task" => "problemReport",
            "title" => "Problem Report",
            "body" => "{$customer->name} has reported a problem.",
            "extraData" => [
                "id" => 1,
                "customer" => $customer,
                "date" => date("Y-m-d")
            ]
        ];
        $ids[] = $user->fcm_token;
        \Util::sendNotification($ids, $message);
        return response()->json($request->all());
    }
}
