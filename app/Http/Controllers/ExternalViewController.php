<?php

namespace App\Http\Controllers;

use App\BusinessDetails;
use App\Invoice;
use Illuminate\Http\Request;

class ExternalViewController extends Controller
{
    public function viewInvoice($invoiceId)
    {
        $invoice = Invoice::with('jobOrder.car.customer')
            ->where("id", $invoiceId)->first();

        $car = $invoice->jobOrder->car;
        $customer = $invoice->jobOrder->car->customer;
        $download = true;
        $business = BusinessDetails::find(1);
        return view('invoice.full_invoice', compact('invoice', 'car', 'customer', 'download', 'business'));
    }
}
