<?php

namespace App\Http\Controllers;

use App\Car;
use App\User;
use Hashids;
use App\Invoice;
use App\JobOrder;
use App\Assement;
use App\Payment;
use Illuminate\Http\Request;
use App\PaymentHistory;
use Illuminate\Queue\Jobs\Job;

class JobOrderController extends Controller
{
    public function index(Request $request)
    {

        $users = User::orderBy("id", "desc")->get();
        if (isset($request->car)) {
            $car = Car::find(decodeId($request->car));
            return view('job_order.index', compact('car', 'users'));
        }
        return view('job_order.index', compact('users'));
    }

    public function initJobOrder(Request $request)
    {
        if(isset($request->car)) {
            $car = Car::find($request->car);
            return response()->json([
                "car_data" => $car,
                "job_orders" => $car->jobOrders()->with('assessment', 'invoice.payment')->orderBy("id", "desc")->get()
            ]);
        }

        return response()->json([
            "job_orders" => JobOrder::with('assessment', 'invoice.payment')->orderBy("id", "desc")->get()
        ]);

    }

    public function addJobOrder(Request $request)
    {

        $car_id = $request->car_id;
        $data = [
            "car_id" => $car_id,
            "date" => $request->date,
            "ref" => date('YmdHis').$car_id,
            "description" => $request->description
        ];

        $jobOrder = JobOrder::updateOrCreate([
            "id" => $request->id
        ], $data);

        $car = Car::find($request->car_id);
        return response()->json([
            "car_data" => $car,
            "job_orders" => $car->jobOrders()->with('assessment', 'invoice.payment')->orderBy("id", "desc")->get()
        ]);
    }

    public function submitAssessment(Request $request)
    {

        $data = $request->all();
        $data['expected_period'] = ($request->expected_num > 1) ?
            $request->expected_num . " " . $request->expected_period . "s" : $request->expected_num . " " . $request->expected_period;

        $data["expected_condition_date"] = calcExpectedConditionPeriod($request->expected_num, $request->expected_period);

        $assesment = Assement::updateOrCreate([
            "job_order_id" => $request->job_order_id
        ], $data);
        return response()->json($assesment);
    }

    public function submitInvoice(Request $request)
    {
        $jobOrder =  JobOrder::find($request->job_order_id);
        $car = $jobOrder->car;
        $customer = $car->customer;
    
        $amount = $request->amount;
        $desc = $request->desc;
        $final_break_down = [];
        for ($i = 0; $i < count($amount); $i++) {
            $final_break_down[] = [
                "desc" => $desc[$i],
                "amount" => ($amount[$i] != null) ? \Util::formatCurrency($amount[$i]) : 0
            ];
        }

        $data = $request->all();
        $data["amount"] = \Util::formatCurrency($request->total_amount);
        $data["description"] = json_encode($final_break_down);
        $data["status"] = (isset($request->final_invoice)) ? 'confirmed' : 'preformal';

        $invoice = Invoice::updateOrCreate([
            "job_order_id" => $request->job_order_id
        ], $data);


        return response()->json(\App\Invoice::with('payment')->where("id", $invoice->id)->first());
    }

    public function addPayment(Request $request)
    {

        $invoice = Invoice::find($request->invoice_id);

        if ($invoice->payment == null) {
            $balance = $invoice->amount - \Util::formatCurrency($request->amount_paid);
        } else {
            $curr_pay = $invoice->payment;
            $balance = $curr_pay->balance - \Util::formatCurrency($request->amount_paid);
        }




        $payment = Payment::updateOrCreate([
            "invoice_id" => $request->invoice_id
        ], [
            "invoice_id" => $request->invoice_id,
            "amount_paid" => \Util::formatCurrency($request->amount_paid),
            "balance" => $balance,
            "date" => $request->date
        ]);

        $paymentHist = PaymentHistory::create([
            "invoice_id" => $request->invoice_id,
            "amount_paid" => \Util::formatCurrency($request->amount_paid),
            "balance" => $balance,
            "date" => $request->date
        ]);


        $invoice = Invoice::with('jobOrder.car.customer')
            ->where("id", $request->invoice_id)->first();

        if($balance <= 0) {
            $invoice->jobOrder->update([
                "status" => "paid"
            ]);
        }


        $car = $invoice->jobOrder->car;
        $customer = $invoice->jobOrder->car->customer;

        $message = "Your payment of UGX {$request->amount_paid} for job order :{$invoice->jobOrder->ref} has been confirmed.";
        sendSms($message, $customer->contact);
        return response()->json(Invoice::with('payment')->where("id", $invoice->id)->first());

    }
}