<?php

namespace App\Http\Controllers;

use App\BusinessDetails;
use Illuminate\Http\Request;

class BusinessController extends Controller
{
    public function index()
    {
        $business = BusinessDetails::find(1);
        if($business) {
            return view('business.index', compact('business'));
        }
        return view('business.index');
    }

    public function addBusiness(Request $request)
    {
        $data = $request->all();
        $business = BusinessDetails::updateOrCreate(["id" => $request->id], $data);
        flash('Data saved')->success()->important();
        return redirect('business');
    }
}
