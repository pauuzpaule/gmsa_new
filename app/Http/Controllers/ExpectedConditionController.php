<?php

namespace App\Http\Controllers;

use App\Assement;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ExpectedConditionController extends Controller
{
    public function index()
    {
        $conditions = Assement::with('jobOrder.car.customer')
            ->where("expected_condition", "!=", null)
            ->whereDate('expected_condition_date', '>', Carbon::now()->toDateString())
            ->orderBy('expected_condition_date', 'asc')->paginate(10);
        return view('job_order.expected_condition', compact('conditions'));
    }
}
