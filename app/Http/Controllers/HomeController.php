<?php

namespace App\Http\Controllers;

use App\Car;
use App\Customer;
use App\JobOrder;
use App\PaymentHistory;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->check()) {
            return redirect()->route('dashboard');
        }
        return view('auth.login');
    }

    public function dashboard()
    {
        $jobOrders = JobOrder::get();
        $customers = Customer::get();
        $cars = Car::get();
        $payments = PaymentHistory::get();

        $byStatus = $jobOrders->groupBy("status");
        $pending = $byStatus->has("pending") ? $byStatus["pending"]->count() : 0;
        $paid = $byStatus->has("paid") ? $byStatus["paid"]->count() : 0;
        $graphData = $this->graphData($paid, $pending);

        return view('home', compact('jobOrders', 'customers', 'cars', 'payments', 'graphData'));
    }

    private function graphData($paid, $unpaid)
    {
        return json_encode([
            "labels" => ["Unpaid", "Paid"],
            "datasets" => [[
                "backgroundColor" => ["#3e95cd", "#8e5ea2"],
                "data" => [$unpaid, $paid]
            ]]
        ]);
    }
}
