<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{

    public function index()
    {
        $profile = auth()->user();
        return view('profile.index', compact('profile'));
    }

    public function updateDetails(Request $request)
    {
        $user = auth()->user()->update($request->all());
        flash("Data Saved")->success()->important();
        return redirect('/profile');
    }

    public function updatePassword(Request $request)
    {
        $user = auth()->user();
        if($request->has('old_password')) {
            if(!Hash::check($request->old_password, $user->password)){
                flash('Old password is incorrect')->error()->important();
                return back();
            }
        }

        $user->update([
            "password" => bcrypt($request->password)
        ]);

        flash("Password update")->success()->important();
        return redirect('/profile');
    }
}
