<?php

namespace App\Http\Controllers;

use App\User;
use App\UserType;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function usersInit() {
        return response()->json([
            "users" => User::orderBy("id", "desc")->get(),
            "user_types" => UserType::get()
        ]);
    }
    public function index(Request $request){
        return view('user.index');
    }

    public function userTypes(Request $request){
        $usertypes = \App\UserType::get();
        return view('user.usertype', compact('usertypes'));
    }

    public function getUser($id) {
        return User::find($id);
    }
    public function addUser(Request $request){
        $data = $request->all();
        if(!$request->has("id")) {
            $data["password"] = bcrypt("password");
        }
        $user = User::updateOrCreate(["id" => $request->id], $data);
        return redirect('/users');
    }

    public function addUserType(Request $request){

        $user_type = \App\UserType::updateOrCreate([
            "id" => $request->id
        ],[
            "name" => $request->name
        ]);

        return redirect()->back();
    }

    public function getUserType(Request $request){
        return response()->json(\App\UserType::find($request->id));
    }
}
