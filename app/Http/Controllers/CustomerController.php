<?php

namespace App\Http\Controllers;

use \App\Car;
use \App\Customer;
use Illuminate\Http\Request;



class CustomerController extends Controller
{
    public function index(Request $request)
    {
        return view("customer.index");
    }

    public function customerCars(Request $request)
    {
        $customerId = $request->customer;
        if($request->has('ajax')) {
            $customer = Customer::find(decodeId($request->customer));
            $cars = $customer->carz;
            return response()->json($cars);
        }
        return view("car.index", compact('customerId'));
    }


    public function customerInit()
    {
        return response()->json([
            "customers" => Customer::with('carz')->get(),
        ]);
    }

    public function addCustomer(Request $request)
    {
        $data = $request->all();
        $customer = Customer::updateOrCreate([
            "id" => $request->id
        ], $data);

        if ($customer->customer_token == null) {
            $customer->update([
                "customer_token" => \Util::generateRandomString(6)
            ]);
        }
        return $this->customerInit();
    }

    public function addCar(Request $request)
    {


        Car::updateOrCreate([
            "id" => $request->id
        ], [
            "customer_id" => decodeId($request->customer_id),
            "type" => $request->type,
            "model" => $request->model,
            "license_plate" => $request->license_plate
        ]);

        $customer = Customer::find(decodeId($request->customer_id));
        return response()->json($customer->carz);
    }
}
