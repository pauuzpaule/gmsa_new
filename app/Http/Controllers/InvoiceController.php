<?php

namespace App\Http\Controllers;

use App\BusinessDetails;
use App\Invoice;
use App\Mail\InvoiceMail;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;

class InvoiceController extends Controller
{
    public function previewInvoice($invoiceId)
    {
        $invoice = Invoice::with('jobOrder.car.customer')
            ->where("id", $invoiceId)->first();

        $car = $invoice->jobOrder->car;
        $customer = $invoice->jobOrder->car->customer;
        $preview = true;
        $business = BusinessDetails::find(1);
        return view('invoice.full_invoice', compact('invoice', 'car', 'customer', 'preview', 'business'));
    }

    public function downloadInvoice($invoiceId)
    {
        $invoice = Invoice::with('jobOrder.car.customer')
            ->where("id", $invoiceId)->first();
        $car = $invoice->jobOrder->car;
        $customer = $invoice->jobOrder->car->customer;
        $business = BusinessDetails::find(1);
        $pdf = PDF::loadView('invoice.full_invoice', compact('invoice', 'car', 'customer', 'business'));
        return $pdf->download('invoice.pdf');
    }

    public function emailInvoice($invoiceId)
    {
        $invoice = Invoice::with('jobOrder.car.customer')
            ->where("id", $invoiceId)->first();
        $car = $invoice->jobOrder->car;
        $customer = $invoice->jobOrder->car->customer;

        $message = (new InvoiceMail($invoice, $customer, $car));
        Mail::to($customer->email)->send($message);
        return "EMAIL SENT";
    }

    public function smsInvoice($invoiceId)
    {
        $invoice = Invoice::with('jobOrder.car.customer')
            ->where("id", $invoiceId)->first();

        $jobOrder = $invoice->jobOrder;
        $car = $invoice->jobOrder->car;
        $customer = $invoice->jobOrder->car->customer;

        $url = url('view-invoice/'.$invoiceId);
        $business = BusinessDetails::find(1);
        $message = "A {$invoice->status} Invoice for job order: {$jobOrder->ref} has been generated \n Go to {$url} to view details or call {$business->contact}";
        sendSms($message, $customer->contact);
        return "Message sent";
    }
}