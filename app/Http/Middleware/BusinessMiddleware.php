<?php

namespace App\Http\Middleware;

use App\BusinessDetails;
use Closure;

class BusinessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $business = BusinessDetails::find(1);
        if(!$business) {
            flash("Add business details.")->info()->important();
            return redirect('/business');
        }
        return $next($request);
    }
}
