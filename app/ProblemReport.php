<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProblemReport extends Model
{
    protected $fillable = [
        "customer_id", "lat", "lng", "description", "image"
    ];
}
