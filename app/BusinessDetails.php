<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessDetails extends Model
{
    protected $fillable = [
        "name", "contact", "address"
    ];
}
