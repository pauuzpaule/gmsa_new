<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    public $fillable  = [
        "job_order_id", "description", "date", "amount", "status"
    ];


    public function getDescriptionAttribute($value)
    {
        return json_decode($value);
    }

    public function payment(){
        return $this->hasOne(Payment::class);
    }

    public function jobOrder()
    {
        return $this->belongsTo(JobOrder::class);
    }
}
