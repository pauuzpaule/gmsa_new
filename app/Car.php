<?php

namespace App;

use Hashids;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = [
        "customer_id", "type", "model", "license_plate"
    ];

    public $appends = [
        "hash_id"
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function gethashIdAttribute()
    {
        return encodeId($this->id);
    }

    public function jobOrders(){
        return $this->hasMany(JobOrder::class);
    }
}
