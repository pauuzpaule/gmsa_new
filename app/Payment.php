<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        "date", "amount_paid", "balance", "invoice_id"
    ];
}
