<?php

trait Util{

    static function generateRandomString($len)
    {
        $alphabet = '1234567890adcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $id = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < $len; $i++) {
            $p = mt_rand(0, $alphaLength);
            $id[] = $alphabet[$p];
        }
        return implode($id);
    }


    static function in_range($number, $min, $max)
    {
        return $number >= $min && $number <= $max;
    }

    static function formatCurrency($amount)
    {
        return str_replace(",", "", $amount);
    }

    static function sendNotification($tokens, $message) {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'priority' => 'high',
            'registration_ids' => $tokens,
            'data' => $message
        );

        $key = env('FCM_KEY');
        $headers = array(
            "Authorization:key = $key",
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === false) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

   

}