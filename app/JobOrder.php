<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class JobOrder extends Model
{
    protected $fillable = [
        "car_id", "ref", "description", "date", "status"
    ];

    public $appends = [
        "date_formatted"
    ];

    public function getDateFormattedAttribute(){
        return Carbon::parse($this->date)->format("d/M/Y");
    }

    public function assessment(){
        return $this->hasOne(Assement::class);
    }

    public function invoice(){
        return $this->hasOne(Invoice::class);
    }

    public function car()
    {
        return $this->belongsTo(Car::class);
    }
}
