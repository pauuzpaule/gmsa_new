<?php

namespace App\Observers;

use App\PaymentApprovedBy;
use App\PaymentHistory;

class PaymentObserver
{
    public function created(PaymentHistory $paymentHistory)
    {
        PaymentApprovedBy::create([
            "user_id" => auth()->user()->id,
            "payment_history_id" => $paymentHistory->id
        ]);
    }
}