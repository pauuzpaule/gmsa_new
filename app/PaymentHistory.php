<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentHistory extends Model
{
    protected $fillable = [
        "date", "amount_paid", "balance", "invoice_id"
    ];
}
