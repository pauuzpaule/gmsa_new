<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        "name", "contact", "image", "customer_token", "fcm_token", "email", "address"
    ];

    public $appends = [
        "hash_id"
    ];

    public function gethashIdAttribute()
    {
        return encodeId($this->id);
    }

    public function carz(){
        return $this->hasMany(Car::class);
    }
}
