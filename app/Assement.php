<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assement extends Model
{
    public $fillable = [
        "job_order_id", "description", "expected_condition", "expected_period", "assessment_date", "assessed_by", "expected_condition_date"
    ];

    public function jobOrder()
    {
        return $this->belongsTo(JobOrder::class);
    }
}
