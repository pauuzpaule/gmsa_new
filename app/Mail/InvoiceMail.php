<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceMail extends Mailable
{
    use Queueable, SerializesModels;

    public $invoice;
    public $customer;
    public $car;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($invoice, $customer, $car)
    {
        $this->invoice = $invoice;
        $this->customer = $customer;
        $this->car = $car;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Invoice")->markdown('emails.invoice');
    }
}
