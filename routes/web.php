<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Barryvdh\DomPDF\Facade as PDF;

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/', 'HomeController@index')->name('/');

Route::group(['middleware' => 'auth'], function () {

    Route::group(['middleware' => 'business'], function () {

        Route::get('dashboard', 'HomeController@dashboard')->name('dashboard');

//user
        Route::get('users', 'UserController@index')->name('users');
        Route::get('users-init', 'UserController@usersInit')->name('users-init');
        Route::post('add-user', 'UserController@addUser')->name('add-user');
        Route::get('get-user/{id}', 'UserController@getUser')->name('get-user');
        Route::get('userTypes', 'UserController@userTypes')->name('userTypes');
        Route::get('getUserType', 'UserController@getUserType')->name('getUserType');
        Route::post('addUserType', 'UserController@addUserType')->name('addUserType');


//customer
        Route::get('customers', 'CustomerController@index')->name('customers');
        Route::get('customerInit', 'CustomerController@customerInit')->name('customerInit');
        Route::post('addCustomer', 'CustomerController@addCustomer')->name('addCustomer');
        Route::get('customerCars/{customer}', 'CustomerController@customerCars')->name('customerCars');
        Route::post('addCar', 'CustomerController@addCar')->name('addCar');


//job order
        Route::get('joborder/{car?}', 'JobOrderController@index')->name('joborder');
        Route::get('initJobOrder', 'JobOrderController@initJobOrder')->name('initJobOrder');
        Route::post('addJobOrder', 'JobOrderController@addJobOrder')->name('addJobOrder');
        Route::post('submitInvoice', 'JobOrderController@submitInvoice')->name('submitInvoice');
        Route::post('submitAssessment', 'JobOrderController@submitAssessment')->name('submitAssessment');
        Route::post('addPayment', 'JobOrderController@addPayment')->name('addPayment');

//invoice
        Route::get('preview-invoice/{invoice}', 'InvoiceController@previewInvoice')->name('preview-invoice');
        Route::get('sms-invoice/{invoice}', 'InvoiceController@smsInvoice')->name('sms-invoice');
        Route::get('email-invoice/{invoice}', 'InvoiceController@emailInvoice')->name('email-invoice');
        Route::get('download-invoice/{invoice}', 'InvoiceController@downloadInvoice')->name('download-invoice');

        //profile
        Route::get('/profile', 'ProfileController@index')->name('profile');
        Route::post('/update-details', 'ProfileController@updateDetails')->name('update-details');
        Route::post('/update-password', 'ProfileController@updatePassword')->name('update-password');

        //conditions
        Route::get('/expected-conditions', 'ExpectedConditionController@index')->name('expected-conditions');

    });

//business
    Route::get('/business', 'BusinessController@index')->name('business');
    Route::post('/add-business', 'BusinessController@addBusiness')->name('add-business');
});

//external view
Route::get('view-invoice/{id}', 'ExternalViewController@viewInvoice');

//mobile [user]
Route::post('mobile/login', 'Mobile\UserController@login');
Route::post('mobile/getCarz', 'Mobile\UserController@getCarz');
Route::post('mobile/getCarJobOrders', 'Mobile\UserController@getCarJobOrders');
Route::post('mobile/getJobOrder', 'Mobile\UserController@getJobOrder');
Route::post('mobile/reportProblem', 'Mobile\UserController@reportProblem');

//moblie [Admin]
Route::post('mobile/getDataForHome', 'Mobile\AdminController@getDataForHome');

Route::get('test', function () {
    $now = \Carbon\Carbon::now();
    $next = \Carbon\Carbon::now()->addDays(5);

    $diff = $next->diffForHumans($now, true);
    dd($diff);
});
