<html>
<head>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css"/>
</head>
<body style="padding: 20px">

<div style="font-size: 28px">
    <span class="company-name">{{$business->name}}</span>
    <span style="float: right">Invoice</span>
    <span style="font-size: 15px"><br> {{$business->contact}}</span>
</div>

<hr style="border: 2px solid #007ACC">

<div class="row">
    <div class="col-sm-3">
        <h5>Invoice To</h5>
        <h6>{{$customer->name}}</h6>
        <h6>{{$customer->address}}</h6>
        <h6>{{$customer->contact}}</h6>
        <h6>CAR: {{ "{$car->type} {$car->model} {$car->license_plate}" }}</h6>
    </div>
    <div class="col-sm-3 offset-sm-6 pull-right">
        <h5>Invoice #INV-{{$invoice->id}}</h5>
        <h6>{{ $invoice->date }}</h6>
        <h6 class="error">STATUS: {{ ucfirst($invoice->status) }}</h6>
    </div>
</div>


<table style="margin-top: 5px" class="table table-bordered printable">
    <thead>
    <tr>
        <td style="width: 10%">#</td>
        <td>Description</td>
        <td style="width: 20%">Price</td>
    </tr>
    </thead>
    <tbody style="backgroud: white;">
    @foreach($invoice->description as $key => $breakDown)
        <tr>
            <td>{{++$key}}</td>
            <td>{{$breakDown->desc}}</td>
            <td>{{ number_format($breakDown->amount) }}</td>
        </tr>
    @endforeach

    </tbody>
    <tfoot>
    <tr>
        <td colspan="2" align="right" style="padding-right: 10px">Total Amount</td>
        <td>{{number_format($invoice->amount)}}</td>
    </tr>
    </tfoot>

</table>

@if(isset($preview))
    <div>
        <a class="btn btn-primary" target="_blank" href="{{url('sms-invoice/'.$invoice->id)}}">Send Message</a>
        @if($customer->email)
            <a class="btn btn-info" target="_blank" href="{{url('email-invoice/'.$invoice->id)}}">Send Email</a>
        @endif
        <a class="btn btn-primary" target="_blank" href="{{url('download-invoice/'.$invoice->id)}}">Download</a>
    </div>
@elseif(isset($download))
    <a class="btn btn-primary" target="_blank" href="{{url('download-invoice/'.$invoice->id)}}">Download</a>
@endif

</body>
</html>
