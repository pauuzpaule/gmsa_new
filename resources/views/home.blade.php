@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="card" >
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <span class="fa fa-2x fa-briefcase"></span>
                        </div>
                        <div class="col-sm-9 text-right">
                            <h5>{{$jobOrders->count()}}</h5>
                            <h6>Job Orders</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card" >
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <span class="fa fa-2x fa-users"></span>
                        </div>
                        <div class="col-sm-9 text-right">
                            <h5>{{$customers->count()}}</h5>
                            <h6>Customers</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card" >
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <span class="fa fa-2x fa-car"></span>
                        </div>
                        <div class="col-sm-9 text-right">
                            <h5>{{$cars->count()}}</h5>
                            <h6>Cars</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card" >
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <span class="fa fa-2x fa-money"></span>
                        </div>
                        <div class="col-sm-9 text-right">
                            <h5>{{ thousandsFormat($payments->sum('amount_paid')) }}</h5>
                            <h6>Payments</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <pie-graph id="pie-graph-1" title="Job Orders" data="{{$graphData}}"></pie-graph>
                </div>
            </div>
        </div>
    </div>
@endsection