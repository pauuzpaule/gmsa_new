<html>
<head>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}"  type="text/css" />
    <style>
        td {
            padding-left: 8px !important;
        }
    </style>
</head>
<body style="padding: 20px">

<div style="font-size: 28px">
    <span class="company-name">Pauuz Auto Mobiles</span>
    <span style="float: right">Invoice</span>
    <span style="font-size: 15px"><br>We Fix everything</span>
</div>

<hr style="border: 2px solid #007ACC">


<table style="width: 100%">
    <tbody>
        <tr>
            <td>
                <h5>Invoice To</h5>
                <h6>{{$customer->name}}</h6>
                <h6>{{$customer->address}}</h6>
                <h6>{{$customer->contact}}</h6>
                <h6>CAR: {{ "{$car->type} {$car->model} {$car->license_plate}" }}</h6>
            </td>

            <td  style="width: 30%">
                <h5>Invoice #INV-{{$invoice->id}}</h5>
                <h6>Invoice date: {{ $invoice->date }}</h6>
                <h6 class="error">Invoice status: {{ ucfirst($invoice->status) }}</h6>
            </td>
        </tr>
    </tbody>
</table>



<table style="margin-top: 5px" class="table table-bordered printable">
    <thead>
    <tr>
        <td style="width: 10%"><span>#</span></td>
        <td>
            <span>Description</span>
        </td>
        <td style="width: 20%">
            <span>Price</span>
        </td>
    </tr>
    </thead>
    <tbody>
    @foreach($invoice->description as $key => $breakDown)
        <tr>
            <td> {{++$key}}</td>
            <td> {{$breakDown->desc}}</td>
            <td> {{ number_format($breakDown->amount) }}</td>
        </tr>
    @endforeach

    </tbody>
    <tfoot>
    <tr>
        <td colspan="2" align="right" style="padding-right: 10px">Total Amount</td>
        <td>{{number_format($invoice->amount)}}</td>
    </tr>
    </tfoot>

</table>


</body>
</html>
