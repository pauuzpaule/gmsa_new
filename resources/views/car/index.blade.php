@extends('layouts.app')
@section('content')

<div class="row" style="padding: 10px 20px;">

    <div class="col-md-12">
        <button @click="showAddCar()" class="btn btn-primary pull-right">+ Add Car</button>
    </div>

    <div class="col-md-12" style="margin-top: 20px;">
        <div class="card">
            <div class="card-header">
                <h6>Cars</h6>
            </div>
            <div class="card-body">
                <table class="table table-striped table-bordered data-table">
                    <thead>
                        <tr>
                            <th style="width: 3%">#</th>
                            <th style="width: 20%">Model</th>
                            <th style="width: 20%">Make</th>
                            <th style="width: 20%">License Plate</th>
                            <th style="width: 5%"> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-if="Object.keys(customercars).length > 0" v-for="(car , index) in customercars">
                            <td>@{{ index+1 }}</td>
                            <td><a href="#">@{{ car.type }}</a></td>
                            <td>@{{ car.model }}</td>
                            <td>@{{ car.license_plate }}</td>
                            <td>
                                <div>
                                    <button @click="gotoUrl('/joborder/'+car.hash_id)" class="btn btn-success btn-sm">Job
                                        Orders</button>
                                    <button @click="showAddCar(index)" class="btn btn-primary btn-sm">edit</button>
                                </div>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

<input id="customerId-forCarz" hidden value="{{$customerId}}">
@include('customer.add_car')

@endsection