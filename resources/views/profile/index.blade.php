@extends('layouts.app')
@section('content')

    <div class="row" style="padding: 10px 20px;">

        <div class="col-md-12" style="margin-top: 20px;">
            <div class="card">
                <div class="card-header">
                    <h6>Profile</h6>
                </div>
                <div class="card-body">
                    @include('flash::message')
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>Change Details</h5>
                            <hr>
                            <form class="form-horizontal form-validate" method="POST" action="{{route('update-details')}}">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="name" class="col-md-4 control-label">Name</label>
                                    <div class="col-md-12">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ isset($profile) ? $profile->name: ''  }}" required >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="contact" class="col-md-4 control-label">Contact</label>

                                    <div class="col-md-12">
                                        <input id="contact" type="text" class="form-control" value="{{ isset($profile) ? $profile->contact: ''  }}" name="contact" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-4 mx-auto">
                                        <button type="submit" class="btn btn-primary btn-block">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-1">
                            <div style="height: 100%; width: 0.5px; background: #cccccc"></div>
                        </div>
                        <div class="col-sm-5">
                            <h5>Change password</h5>
                            <hr>
                            <form class="form-horizontal form-validate-password" method="POST" action="{{route('update-password')}}">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="password" class="col-md-12 control-label">Old Password</label>
                                    <div class="col-md-12">
                                        <input id="old_password" type="password" class="form-control" name="old_password"  required >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="col-md-12 control-label">New password</label>

                                    <div class="col-md-12">
                                        <input id="password" type="password" class="form-control" name="password" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="confirm_password" class="col-md-12 control-label">Confirm password</label>

                                    <div class="col-md-12">
                                        <input id="confirm_password" type="password" class="form-control" name="confirm_password" required>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-4 mx-auto">
                                        <button type="submit" class="btn btn-primary btn-block">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection
