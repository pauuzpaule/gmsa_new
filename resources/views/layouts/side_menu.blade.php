<!-- Left Sidebar -->
	<div class="left main-sidebar">
	
		<div class="sidebar-inner leftscroll">

			<div id="sidebar-menu">
        
			<ul>

					<li class="submenu">
						<a class="{{preg_match('/dashboard/', strtolower(url()->current()))  ? 'active' : ''}}" href="{{ route('dashboard') }}">
							<i class="fa fa-fw fa-dashboard"></i><span> Dashboard </span> </a>
                    </li>

					<li class="submenu">
                        <a class="{{preg_match('/customers/', strtolower(url()->current()))  ? 'active' : ''}}" href="{{ route('customers') }}">
							<i class="fa fa-fw fa-users"></i><span> Customers </span> </a>
                    </li>

					<li class="submenu">
                        <a class="{{preg_match('/joborder/', strtolower(url()->current()))  ? 'active' : ''}}" href="{{ url('joborder') }}">
							<i class="fa fa-fw fa-briefcase"></i><span> Job Order</span> </a>
					</li>

					<li class="submenu">
                        <a class="{{preg_match('/expected-conditions/', strtolower(url()->current()))  ? 'active' : ''}}" href="{{ url('expected-conditions') }}">
							<i class="fa fa-fw fa-gear"></i><span> Conditions</span> </a>
					</li>

					<li class="submenu">
                        <a class="{{preg_match('/users/', strtolower(url()->current()))  ? 'active' : ''}}" href="{{ url('users') }}">
							<i class="fa fa-fw fa-user"></i><span> Users</span> </a>
					</li>

					<li class="submenu">
                        <a class="{{preg_match('/business/', strtolower(url()->current()))  ? 'active' : ''}}" href="{{ url('business') }}">
							<i class="fa fa-fw fa-building-o"></i><span> Business Details</span> </a>
					</li>
				
					
            </ul>

            <div class="clearfix"></div>

			</div>
        
			<div class="clearfix"></div>

		</div>

	</div>
	<!-- End Sidebar -->