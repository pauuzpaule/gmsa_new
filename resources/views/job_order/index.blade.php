@extends('layouts.app')
@section('content')

<div class="row" style="padding: 10px 20px;">

    @isset($car)
    <input hidden value="{{ $car->id }}" id="car-id">
    <div class="col-md-12">
        <button @click=" showJobOrder()" class="btn btn-primary pull-right">+ Add Job Order</button>
    </div>
    @endisset


    <div class="col-md-12" style="margin-top: 20px;">
        <div class="card">
            <div class="card-header">
                Job Orders <span v-if="selected_car.model">for @{{selected_car.model}}</span>
            </div>
            <div class="card-body">
                <table class="table table-striped table-bordered data-table">
                    <thead>
                        <tr>
                            <th style="width: 1%">#</th>
                            <th>REF .No</th>
                            <th>Description</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-if="Object.keys(job_orders).length > 0" v-for="(job_order, index) in job_orders">
                            <td>@{{ index +1 }}</td>
                            <td>@{{ job_order.ref }}</td>
                            <td>@{{ job_order.description }}</td>
                            <td>@{{ job_order.date_formatted }}</td>
                            <td style="min-width: 45%">

                                    <a @click.prevent.stop="showAssesment(job_order.id, index)" href="#"
                                        class="btn btn-primary btn-sm">+ Add Assessment</a>
                                    <a v-if="job_order.assessment" @click.prevent.stop="showInvoice(job_order.id, index)" href="#"
                                        class="btn btn-success btn-sm">
                                        <span v-if="job_order.invoice && job_order.invoice.status == 'confirmed'">+ Add Payment </span>
                                        <span v-else>+ Add Invoice </span>
                                    </a>
                                    <a v-if="job_order.invoice" @click.prevent="previewInvoice(job_order.invoice.id)" href="#"
                                        class="btn btn-success btn-sm">Preview Invoice</a>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

@include('job_order.modal.add_job_order')
@include('job_order.modal.add_assesment')
@include('job_order.modal.invoice')
@include('job_order.modal.add_report')
@include('job_order.modal.payment')
@endsection