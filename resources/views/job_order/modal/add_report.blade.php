<div id="modal-report" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Report</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form id="form-add-job_order">
          {{ csrf_field() }}
            <div class="form-group">
                <label>Date:</label>
                <input name="type" type="text" class="form-control datepicker" readonly required>
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control"  required></textarea>
            </div>
           
        </form>
      </div>
      <div class="modal-footer">
        <button @click="submitJobOrder" type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>