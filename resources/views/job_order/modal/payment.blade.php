<div id="modal-payment" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Payment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form id="form-payment">

           <input hidden name="invoice_id" :value="invoice.id"> 
          {{ csrf_field() }}

          <div class="form-group">
                <label>Date:</label>
                <input id="p-date" name="date" type="text" class="form-control datepicker"  readonly required>
            </div>

          <div class="form-group">
                <label>Outstanding Balance:</label>
                <input style="color: red; font-size: 18px;" name="the_balance" type="text" :value="(invoice.payment == null) ? invoice.amount : invoice.payment.balance | toMoney" class="form-control"  readonly required>
            </div>

            <div class="form-group">
                <label>Amount Paid:</label>
                <input id="p-amount-paid" name="amount_paid" type="text" class="form-control currency"  required>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button @click="confirmPayment()" type="button" class="btn btn-primary">Confirm Payment</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>