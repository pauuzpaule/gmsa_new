<div id="modal-assesment" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Assesment </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form id="form-add-assesment">
          {{ csrf_field() }}
           
            <input name="job_order_id" :value="job_order_id" hidden>
            <div class="form-group">
                <label>Date:</label>
                <input name="assessment_date" :value="assesment.assessment_date" type="text" class="form-control datepicker" readonly required>
            </div>


            <div class="form-group">
                <label>Description</label>
                <textarea name="description" class="form-control"  :value="assesment.description" required></textarea>
            </div>

            <div class="form-group">
                <label>Expected Condition:</label>
                <textarea name="expected_condition" :value="assesment.expected_condition" class="form-control"></textarea>
            </div>

            <div class="form-group">
              <label>Expected Condition period</label>
              <div class="row">

                <div class="col-md-3">
                  <select :value="(assesment.expected_period) ? assesment.expected_period.split(' ')[0] : ''"  name="expected_num" class="form-control">
                    {{--  :value="assesment.expected_period.split(' ')[0]"  --}}
                    <option value="">--select period--</option>
                     @foreach(range(1,12) as $num)
                        <option value="{{ $num }}">{{ $num }}</option>
                     @endforeach
                  </select>
                </div>

                <div class="col-md-4">
                  <select  :value="(assesment.expected_period) ? assesment.expected_period.split(' ')[1].replace('s','') : ''"  name="expected_period" class="form-control">
                    {{--  :value="assesment.expected_period.split(' ')[1].replace('s','')"  --}}
                    <option value="">--select period--</option>
                    <option value="day">day(s)</option>
                    <option value="week">week(s)</option>
                    <option value="month">month(s)</option>
                    <option value="year">year(s)</option>
                  </select>
                </div>

              </div>
            </div>

            <div class="form-group">
                <label>Assesed By:</label>
                 <select name="assessed_by" :value="assesment.assessed_by" required class="form-control">
                   <option value="">--select Technician--</option>
                     @foreach($users as $user)
                         <option value="{{$user->id}}">{{$user->name}}</option>
                         @endforeach
                  </select>
            </div>
           
        </form>
      </div>
      <div class="modal-footer">
        <button v-if="job_order_data.invoice == null || ( job_order_data.invoice && job_order_data.invoice.status != 'confirmed')" @click="submitAssesment" type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>