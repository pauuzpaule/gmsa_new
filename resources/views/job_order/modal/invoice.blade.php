<div id="modal-invoice" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Invoice </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div v-if="invoice.status == 'confirmed'" class="ribbon ribbon-top-left"><span class="green">Final</span></div>
        <div v-else class="ribbon ribbon-top-left"><span class="red">Pre-formal</span></div>
      </div>
      <div class="modal-body">
         <form id="form-add-invoice">
          {{ csrf_field() }}
            
          <input name="job_order_id" :value="job_order_id" hidden>
          
            <div class="form-group">
                <label>Date:</label>
                <input name="date" :value="invoice.date"  type="text" class="form-control datepicker" readonly required>
            </div>

            {{--  <div class="form-group">
                <label>Description</label>
                <textarea name="description" class="form-control"  required></textarea>
            </div>  --}}

            <div class="form-group">
                <label>Description: <a @click.prevent.stop="addOrmoveBreakDown()" class="btn btn-rounded btn-primary">+</a></label>
                <div v-for="(item, index) in breakDown" class="row" style="margin-bottom: 5px">
                    <div class="col-md-5"><input placeholder="description" :id="'bd-'+index" :value="item.desc" @input="inputDesc($event, index)" name="desc[]" type="text" class="form-control" required></div>
                    <div class="col-md-5"><input placeholder="amount" :id="'ba-'+index" @keyup.delete="onInputDelete($event, index)" :value="item.amount | toMoney"  @input="inputTotalCalc($event, index)" name="amount[]" type="text" class="form-control" required></div>
                    <div class="col-md-2"><button v-if="index != 0" @click.prevent.stop="addOrmoveBreakDown(index)" class="btn btn-rounded btn-danger">-</button></div>
                </div>
                
            </div>

            <div class="form-group">
                <label>Total Amount:</label>
                <input style="font-size:20px;" name="total_amount" :value="invoice_total | toMoney" type="text" class="form-control" readonly required>
            </div>

            <div v-if="invoice.status != 'confirmed'" class="form-group">
                <label style="" class="cr-label">
                    <input type="checkbox" id="final_invoice" name="final_invoice" :checked="invoice.status == 'confirmed'">
                    <span class="label-text">Final Invoice</span>
                </label>
            </div>
           
        </form>
      </div>
      <div class="modal-footer">
        <button @click="submitInvoice()" v-if="invoice.status != 'confirmed'"  type="button" class="btn btn-primary">Save changes</button>
        <button @click="showPayment()" v-if="invoice.status == 'confirmed'" type="button" class="btn btn-success">Payment</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>