@extends('layouts.app')
@section('content')

    <div class="row" style="padding: 10px 20px;">

        <div class="col-md-12" style="margin-top: 20px;">
            <div class="card">
                <div class="card-header">
                    Expected Conditions <span class="float-right">{{$conditions->links()}}</span>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered data-table">
                        <thead>
                        <tr>
                            <th style="width: 1%">#</th>
                            <th>Car</th>
                            <th>Description</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($conditions as $key => $condition)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>
                                        <h6>{{$condition->jobOrder->car->type}}</h6>
                                        <h6 class="text-muted">{{$condition->jobOrder->car->license_plate}}</h6>
                                    </td>
                                    <td>{{$condition->expected_condition}}</td>
                                    <td>
                                        <h6>{{formatDate($condition->expected_condition_date)}}</h6>
                                        <h6 class="text-muted">Happening in {{happeningIn($condition->expected_condition_date)}}</h6>
                                    </td>
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

@endsection