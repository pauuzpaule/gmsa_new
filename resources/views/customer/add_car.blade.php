<div id="add-car" class="modal fade " tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Car Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <form id="form-add-car">
          {{ csrf_field() }}
            <input name="id" :value="selected_car.id" hidden>
            <input name="customer_id" :value="selected_car.customer_id" hidden>
            <div class="form-group">
                <label>Car Type</label>
                <input name="type" :value="selected_car.type" type="text" class="form-control" required>
            </div>
            <div class="form-group">
                <label>Car Model</label>
                <input name="model" :value="selected_car.model" type="text" class="form-control" required>
            </div>
            <div class="form-group">
                <label>License plate</label>
                <input name="license_plate" :value="selected_car.license_plate" type="text" class="form-control" required>
            </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" @click="submitAddCar" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>