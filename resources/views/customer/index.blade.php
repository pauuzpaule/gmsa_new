@extends('layouts.app')
@section('content')

<div class="row" style="padding: 10px 20px;">

    <div class="col-md-12">
        <button @click="showAddCustomer()" class="btn btn-primary pull-right">+ Add Customer</button>
    </div>

    <div class="col-md-12" style="margin-top: 20px;">
        <div class="card">
            <div class="card-header">
                <h6>Customer</h6>
            </div>
            <div class="card-body">
                <table class="table table-striped table-bordered data-table">
                    <thead>
                        <tr>
                            <th style="width: 3%">#</th>
                            <th>Details</th>
                            <th>Contact</th>
                            <th>Address</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-if="Object.keys(customers).length > 0" v-for="(customer , index) in customers">
                            <td>@{{ index+1 }}</td>
                            <td><a href="#" @click="showAddCustomer(index)">
                                    @{{ customer.name }}</a><br>
                                  <span class="text-muted">@{{ customer.email }}</span>
                            </td>
                            <td>@{{ customer.contact }}</td>
                            <td>@{{ customer.address }}</td>
                            <td style="width: 20%">
                                <a @click.prevent="showAddCustomer(index)" href="#" class="btn btn-success btn-sm">update</a>
                                <a @click.prevent="gotoUrl('/customerCars/'+customer.hash_id)" href="#" class="btn btn-primary btn-sm">cars</a>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

@include('customer.add_customer')
@include('customer.add_car')
@endsection