<div id="add-customer" class="modal fade " tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Customer Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <form id="form-add-customer">
          {{ csrf_field() }}
          <input name="id" hidden :value="selected_customer.id"> 
            <div class="form-group">
                <label>Customer Name</label>
                <input :value="selected_customer.name" name="name" type="text" class="form-control" required>
            </div>
            <div class="form-group">
                <label>Customer Contact</label>
                <input :value="selected_customer.contact" name="contact" type="text" class="form-control" required>
            </div>

          <div class="form-group">
                <label>Customer Email</label>
                <input :value="selected_customer.email" name="email" type="text" class="form-control" >
            </div>

          <div class="form-group">
                <label>Customer Address</label>
                <input :value="selected_customer.address" name="address" type="text" class="form-control" required>
            </div>
        </form>

      </div>
      <div class="modal-footer">
        <button @click="submitAddCustomer" type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>