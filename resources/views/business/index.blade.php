@extends('layouts.app')
@section('content')

    <div class="row" style="padding: 10px 20px;">

        <div class="col-md-12" style="margin-top: 20px;">
            <div class="card">
                <div class="card-header">
                    <h6>Business Details</h6>
                </div>
                <div class="card-body">
                    @include('flash::message')
                    <form class="form-horizontal form-validate" method="POST" action="{{ route('add-business') }}">
                        {{ csrf_field() }}

                        @if(isset($business))
                            <input hidden name="id" value="{{$business->id}}">
                            @endif

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control" name="name" value="{{ isset($business) ? $business->name: ''  }}" required >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="contact" class="col-md-4 control-label">Contact</label>

                            <div class="col-md-12">
                                <input id="contact" type="text" class="form-control" value="{{ isset($business) ? $business->contact: ''  }}" name="contact" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="col-md-4 control-label">Address</label>

                            <div class="col-md-12">
                                <input id="address" type="text" class="form-control" value="{{ isset($business) ? $business->address: ''  }}" name="address" required>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-4 mx-auto">
                                <button type="submit" class="btn btn-primary btn-block">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection
