<div id="modal-user" class="modal fade " tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">User Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-validate" id="form-add-user" action="{{route('add-user')}}" method="post">
          {{ csrf_field() }}
            <input v-if="user.id" :value="user.id" hidden name="id">
            <div class="form-group">
                <label>Name</label>
                <input :value="user.name" name="name" type="text" class="form-control" required>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input :value="user.email" name="email" type="email" class="form-control" required>
            </div><div class="form-group">
                <label>Contact</label>
                <input name="contact" :value="user.contact" type="text" class="form-control" required>
            </div>
            <div class="form-group">
                <label>User Type</label>
                <select name="user_type" class="form-control" required>
                    <option v-if="user.user_type" :value="user.user_type" selected>@{{ user.user_type }}</option>
                    <option v-else disabled selected> --Select user type--</option>
                    <option value="ADMIN">ADMIN</option>
                    <option value="TECHNICIAN">TECHNICIAN</option>
                </select>
            </div>
        </form>

      </div>
      <div class="modal-footer">
        <button type="submit" @click="saveUser" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>