@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <button @click="showAddUser()" class="btn btn-primary pull-right">+ Add User</button>
        </div>

        <div class="col-md-12" style="margin-top: 20px;">
            <div class="card">
                <div class="card-header">
                    <h6>Users</h6>
                </div>
                <div class="card-body">
                    <table style="width: 100%" class="table table-striped table-bordered data-table">
                        <thead>
                        <tr>
                            <td style="width: 13%">#</td>
                            <td>User</td>
                            <td>Type</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr  v-for="(user , index) in users">
                            <td>@{{ index+1 }}</td>
                            <td>
                                <a href="#" @click="getUser(user.id)">@{{ user.name }}</a><br>
                                @{{ user.contact }}
                            </td>
                            <td>@{{ user.user_type }}</td>
                            <td style="width: 20%">
                                <a @click.prevent="getUser(user.id)" href="#" class="btn btn-success btn-sm">update</a>
                                <a @click.prevent="gotoUrl('/customerCars/'+customer.hash_id)" href="#" class="btn btn-primary btn-sm">Delete</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>


    </div>

    @include('user.add_user')
    @include('user.add_user_type')
@endsection