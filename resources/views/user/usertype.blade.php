@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <button style="margin-right: 15px;" @click="showAddUserType()" class="btn btn-primary pull-right">+ Add User TYpe</button>
        </div>

        <div style="margin-top: 20px;" class="col-md-12">
            <table class="table table-striped data-table">
                <thead>
                    <tr>
                        <td style="width: 13%">#</td>
                        <td>User Type</td>
                        <td style="width: 10%"></td>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($usertypes as $key => $usertype)

                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $usertype->name }}</td>
                            <td>
                                <button @click="showAddUserType({{$usertype->id}})" type="button" class="btn btn-success">
                                    <span class="fa fa-pencil" aria-hidden="true"></span>
                                </button>
                                <button  type="button" class="btn btn-danger">
                                    <span class="fa fa-trash" aria-hidden="true"></span>
                                </button>
                            </td>
                        </tr>
                        
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @include('user.add_user_type')
@endsection
    
