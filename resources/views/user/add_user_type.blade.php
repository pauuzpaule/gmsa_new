<div id="modal-user-type" class="modal fade " tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">User Type</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <form method="post" action="addUserType" id="form-add-user-type">
          {{ csrf_field() }}

            <input hidden  name="id" :value="usertype.id" >
            <div class="form-group">
                <label>User Type: </label>
                <input name="name" :value="usertype.name"  type="text" class="form-control" required>
            </div>
        </form>

      </div>
      <div class="modal-footer">
 
        <button type="button" @click="submitUserType" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>