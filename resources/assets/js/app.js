
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');

import Customer from './mixinz/customer.js'
import JobOrder from './mixinz/job_order.js'
import Invoice from './mixinz/invoice.js'
import User from './mixinz/user'

window.Vue = require('vue');
import iziToast from './plugins/izitoast'
Vue.use(iziToast);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('line-graph', require('./components/graph/LineGraph.vue'));
Vue.component('bar-graph', require('./components/graph/BarGraph.vue'));
Vue.component('pie-graph', require('./components/graph/PieGraph.vue'));

const app = new Vue({
    el: '#main',
    mixins:[
        Customer, JobOrder , Invoice, User
    ],
    mounted(){
        $(document).ajaxSend(function () {
            $('button').attr('disabled', true)
        });

        $(document).ajaxComplete(function () {
            $('button').attr('disabled', false)
        });
    },
    data:{
        app_url: base_url
    },
    filters: {
        uppercase: function (v) {
            return v.toUpperCase();
        },
        toMoney: function (v) {
            return parseFloat(v).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').split(".")[0]
        },
    },
    methods:{
           gotoUrl(url) {
               window.location.href = base_url+url
           }, 
           updateTable(callback) {
               let app = this;
               $(".data-table").hide();
               $(".data-table").DataTable().destroy();
               setTimeout(function () {
                   if (callback) {
                       callback;
                       $(".data-table").DataTable();
                   }
                   $(".data-table").fadeIn();
               }, 800)

           },
           isNumber(n) {
               return !isNaN(parseFloat(n)) && isFinite(n);
           },
           toastMessage(message, type = 'success', position = 'topRight') {
               var app = this
               if (type == 'error') {
                   app.$iziToast.error({
                       position: position,
                       message: message,
                       progressBar: true,
                       timeout: 8000,
                   })
               } else {
                   app.$iziToast.success({
                       position: position,
                       message: message,
                       progressBar: true,
                       timeout: 8000,
                   })
               }
           },
          popupWindow(url, title, w, h) {
            // Fixes dual-screen position                         Most browsers      Firefox
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (w / 2)) + dualScreenLeft;
            var top = ((height / 2) - (h / 2)) + dualScreenTop;
            var newWindow = window.open(url, title, 'model=yes,menubar=no,status=no,toolbar=no,location=no,width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

            // Puts focus on the newWindow
            if (window.focus) {
                newWindow.focus();
                newWindow.document.title = title
            }
        },
        hexToRgb(hex, opacity = 0.1) {
            let rgb = hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, (m, r, g, b) => '#' + r + r + g + g + b + b)
                .substring(1).match(/.{2}/g)
                .map(x => parseInt(x, 16));
            return "rgba("+rgb.toString()+","+opacity.toString()+")";
        }
    }
});
