$(document).ready(function(){
    $(".data-table").DataTable()


    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
    });

    $('.datepickerYear').datepicker({
        format: 'yyyy',
        viewMode: "years",
        minViewMode: "years",
        autoclose: true,
    });


    $("#form-add-customer").validate();
    $("#form-add-job_order").validate();
    $("#form-add-invoice").validate({});
    $("#form-add-assesment").validate({});
    $("#form-payment").validate({});
    $(".form-validate").validate();

    $(".form-validate-password").validate({
        rules : {
            password : {
                minlength : 5
            },
            confirm_password : {
                minlength : 5,
                equalTo : "#password"
            }
        }
    });

    $('.currency').maskMoney({
        precision: 0
    });

});