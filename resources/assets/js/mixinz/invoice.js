const invoice = {

    methods:{
        showPayment(){
            $("#modal-invoice").modal('hide')

            $('#p-date').val('')
            $('#p-amount-paid').val('')
            $("#modal-payment").modal('show')
        },
        confirmPayment(){
            var app = this
            app.formSubmmision('form-payment', 'addPayment', (data) => {
                $("#modal-payment").modal('hide')
                app.job_orders[app.job_order_index].invoice = data
            })
        }
    }

}

export default invoice