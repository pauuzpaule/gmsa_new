const user = {

    mounted() {
        let app = this;
        if(window.location.href.includes('users')){
            $.ajax({
                url: base_url + "/users-init",
                success(data){
                    app.user_types = data.user_types;
                    app.users = data.users;
                    app.updateTable(app.users = data.users);
                }
            })
        }
    },
    data(){
        return {
            user: {},
            users: [],
            user_types: [],
            usertype:{}
        }
    },
    methods:{
        showAddUser(){
            this.user = {};
            $("#modal-user").modal("show")
        },
        saveUser() {
            let form = $("#form-add-user");
            if(form.valid()) {
                form.submit();
            }
        },
        getUser(id) {
            let app = this;
            app.user = {};
            $.ajax({
                url: base_url+ "/get-user/"+id,
                success(data) {
                    app.user = data;
                    $("#modal-user").modal("show");
                }
            })
        },

        showAddUserType(id = null){
            let app = this;
            app.usertype = {};
            new Promise(() => {
                if(id != null){
                    $.ajax({
                        url: base_url +"/getUserType",
                        data:{
                            id : id
                        },
                        success(data){
                            app.usertype = data
                        }
                    })
                }
                resolve()
            }).then($("#modal-user-type").modal("show"))
            
        },
        submitUserType(){
            let app = this;
            $("#form-add-user-type").submit()
            // app.formSubmmision('form-add-user-type', 'addUserType', (data) => {
            //     app.user_types = data
            //     //console.log(data)
            // })
        }
    }

};

export default user