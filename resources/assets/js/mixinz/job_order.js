import swal from "sweetalert2";

const jobOrder = {

    mounted(){

        let app =this;
        if(window.location.href.includes('joborder')){
            let car_id = $("#car-id");
            if(car_id.length > 0){
                app.initJobOrder(car_id.val());
            }else {
                app.initJobOrder();
            }
           
        }

    },
    data(){
        return {
            breakDown:[{"desc": '', "amount": 0}],
            invoice_total : 0,
            selected_car: {},
            selected_job_order: {},
            job_orders: {},
            job_order_id: "",
            job_order_data: {},
            assesment: {},
            invoice: {},
            job_order_index : ""
        }
    },
    methods:{
        initJobOrder(car_id = null) {
            let app = this;
            $.ajax({
                url: base_url + '/initJobOrder',
                data:{
                    car: car_id
                },
                success(data) {
                    if(car_id != null) {
                        app.selected_car = data.car_data;
                    }
                    app.updateTable(app.job_orders = data.job_orders)
                },
                error(error){
                    //window.location.reload(true)
                }
            })
        },
        gotoJobOrder(car){
            window.location.href = base_url+'/joborder/'+car
        },
        showJobOrder(id = null){
            var app = this;
            if(id != null){

            }
            $("#modal-job-order").modal('show')
        },
        showAssesment(job_order_id, index){
            let app = this;
            app.job_order_id = job_order_id;
            app.job_order_index = index;
            app.job_order_data = app.job_orders[index];
            console.log(app.job_order_data);
            app.assesment = (app.job_orders[index].assessment != null) ? app.job_orders[index].assessment : {};
            $("#modal-assesment").modal('show');
        },
        showInvoice(job_order_id, index){
            let app = this;
            app.job_order_id = job_order_id
            app.job_order_index = index
            app.invoice = (app.job_orders[index].invoice != null) ? app.job_orders[index].invoice : {};
            app.breakDown = (app.job_orders[index].invoice != null) ? app.job_orders[index].invoice.description : [{
                "desc": '',
                "amount": 0
            }];
            app.invoice_total = (app.job_orders[index].invoice != null) ? app.job_orders[index].invoice.amount : 0;
           
            $("#modal-invoice").modal('show');
        },
        submitJobOrder(){
            let app = this;
            let form = $("#form-add-job_order");
            if (form.valid()) {
                $.ajax({
                    url: base_url +"/addJobOrder",
                    type: 'post',
                    data: form.serialize(),
                    success(data){
                        app.updateTable(app.job_orders = data.job_orders);
                        $("#modal-job-order").modal('hide');
                    }
                })
            }
        },
        addOrmoveBreakDown(index = null){
            let app = this;

            if(index != null) {
                if (index > -1) {
                    app.breakDown.splice(index, 1);
                }

                let total = 0;
                for (let i = 0; i < app.breakDown.length; i++) {
                     let value = (app.isNumber(app.breakDown[i].amount)) ? parseFloat(app.breakDown[i].amount) : 0;
                     total = (total + value);
                }

                app.invoice_total = total

            }else{
                app.breakDown.push({ "desc" : '', "amount": 0})
            }

            
        },
        inputTotalCalc(e, index){
            let app = this;
            let total = 0;

            let value = e.target.value;
            let old_value = value.substring(0, value.length - 1);
            let new_char = value.slice(-1);

            

            if(!app.isNumber(new_char)){
                 //app.breakDown[index].amount = parseFloat(old_value.replace(",", ""))
                 $("#ba-"+index).val(old_value);
                 return;
            }

            if (app.isNumber(e.target.value.replace(",", ""))) {

                
                 let targetVal = parseFloat(e.target.value.replace(",",""));

                 app.breakDown[index].amount = targetVal;
                 if (app.breakDown.length > 1) {
                     for (let i = 0; i < app.breakDown.length; i++) {
                         if (i != index) {
                            let value = (app.isNumber(app.breakDown[i].amount)) ? parseFloat(app.breakDown[i].amount) : 0;
                             total = (total + value);
                            
                         }
                     }
                 }
                 app.invoice_total = (targetVal + total);

            }
           
        },
        inputDesc(e, index){
            let app = this;
            app.breakDown[index].desc = e.target.value
        },
        onInputDelete(e, index){
            let app = this;
            if (e.target.value.length == 0) {
                app.breakDown[index].amount = 0;

                 let total = 0;
                 for (let i = 0; i < app.breakDown.length; i++) {
                     let value = (app.isNumber(app.breakDown[i].amount)) ? parseFloat(app.breakDown[i].amount) : 0;
                     total = (total + value);
                 }

                 app.invoice_total = total
            }

           
        },
        submitInvoice(){
            let app = this;
            if ($("#form-add-invoice").valid()){
                if ($("#final_invoice").is(':checked')) {
                    swal({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: "Yes, Mark as final Invoice",
                        showLoaderOnConfirm: true,
                        allowOutsideClick: false,
                        preConfirm: function () {
                            return new Promise(function (resolve) {
                                app.formSubmmision('form-add-invoice', 'submitInvoice', (data) => {
                                    $("#modal-invoice").modal('hide');
                                    app.job_orders[app.job_order_index].invoice = data;
                                       resolve();

                                })
                             
                            });
                        }
                    }).then((result) => {
                        if (result.value) {

                        }

                    })
                } else {
                    app.formSubmmision('form-add-invoice', 'submitInvoice', (data) => {
                        $("#modal-invoice").modal('hide');
                        app.job_orders[app.job_order_index].invoice = data
                    })
                }
            }
            
            
        },
        previewInvoice(invoice) {
            let url = base_url+"/preview-invoice/"+invoice;
            this.popupWindow(url, "Invoice", 800, 600);
        },
        submitAssesment(){
            let app = this;
            app.formSubmmision('form-add-assesment', 'submitAssessment', (data) => {
                app.job_orders[app.job_order_index].assessment = data;
                 $("#modal-assesment").modal('hide')
            })
        },
        showAddReport(){
            $("#modal-report").modal('show')
        },
        formSubmmision(form, url, callback){
            let app =this;
            let submitForm = $("#"+form);
            if (submitForm.valid()) {
                $.ajax({
                    url: base_url + "/"+url,
                    type: 'post',
                    data: submitForm.serialize(),
                    success(data) {
                        app.toastMessage("Done !!");
                        callback(data)
                    }
                })
            }
        }
    }

};

export default jobOrder