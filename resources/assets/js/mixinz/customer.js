const customer = {

    mounted(){
        let app = this;
        if(window.location.href.includes('customers')){
            $.ajax({
                url: base_url + "/customerInit",
                success(data){
                    app.customers = data.customers;
                    app.updateTable(app.customers = data.customers)
                }
            })
        }

        if($("#customerId-forCarz").length > 0) {
            var customerId = $("#customerId-forCarz").val();
            app.customerId = customerId;
            app.getCustomerCars(customerId)
        }
    },
    data(){
        return{
            customerId: null,
            customers: {},
            customercars: {},
            selected_customer: {},
            selected_car: {}
        }
    },
    methods:{
        getCustomerCars(customerId){
            let app = this;
            $.ajax({
                url: base_url+ '/customerCars/'+customerId,
                data: {
                    'ajax' : true
                },
                success(data) {
                    app.updateTable(app.customercars = data)
                }
            });
        },
        showAddCustomer(index){
            var app = this
            app.selected_customer = {}
            if(Object.keys(app.customers).length > 0 && index != null){
                app.selected_customer = app.customers[index]
            }
            $("#add-customer").modal('show')
        },
        showAddCar(index) {
            var app = this
            app.selected_car = {}
            if(index != null) {
                app.selected_car = app.customercars[index]
            }
            
            if(app.customerId != null) {
                app.selected_car["customer_id"] = app.customerId;
            }
            $("#add-car").modal('show')
        },
        submitAddCustomer(){
            var app = this;
            if($("#form-add-customer").valid()){
                $.ajax({
                    url: base_url + '/addCustomer',
                    type:'post',
                    data: $("#form-add-customer").serialize(),
                    success(data){
                        $("#add-customer").modal('hide')
                        app.customers = data.customers
                        app.updateTable(app.customers = data.customers)
                    }
                })
            }
        },
        submitAddCar(){
            var app =this
            if($("#form-add-car").valid()){
                 $.ajax({
                     url: base_url + '/addCar',
                     type: 'post',
                     data: $("#form-add-car").serialize(),
                     success(data) {
                         $("#add-car").modal('hide')
                         app.updateTable(app.customercars = data)
                     }
                 })
            }
        }
    }

}
export default customer;