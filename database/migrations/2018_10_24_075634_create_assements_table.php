<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("job_order_id");
            $table->unsignedInteger("assessed_by");
            $table->longText("description");
            $table->longText("expected_condition")->nullable();
            $table->string("expected_period")->nullable();
            $table->date("expected_condition_date")->nullable();
            $table->date("assessment_date");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assements');
    }
}
